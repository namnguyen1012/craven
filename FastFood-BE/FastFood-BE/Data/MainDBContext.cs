﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FastFood_BE.Model;

namespace FastFood_BE.Data
{
    public class MainDBContext : DbContext
    {
        public MainDBContext (DbContextOptions<MainDBContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CartDetail>().HasKey(cd => new { cd.CartId, cd.ProductId });
        }

        public DbSet<FastFood_BE.Model.Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CartDetail> CartDetails { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}
