﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.DTOs
{
    public class CartDetailDTO
    {
        public int CartId { get; set; }
        public int ProductId { get; set; }
    }
}
