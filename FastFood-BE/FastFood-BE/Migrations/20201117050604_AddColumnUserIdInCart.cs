﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FastFood_BE.Migrations
{
    public partial class AddColumnUserIdInCart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "userId",
                table: "Carts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "userId",
                table: "Carts");
        }
    }
}
