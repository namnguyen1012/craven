﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding
{
    public class CartBindingModel
    {
        public int ProductId { get; set; }
        public int CartId { get; set; }
        public string Place { get; set; }
        public int Quantity { get; set; }
        public string PickupTime { get; set; }
        public string PickupType { get; set; }
        public string CartNote { get; set; }
        public DateTime CartDate { get; set; }
        public float TotalPrice { get; set; }
        public string Status { get; set; }

        public Customer customer { get; set; }
        public ICollection<CartDetail> CartDetails { get; set; }
    }
}
