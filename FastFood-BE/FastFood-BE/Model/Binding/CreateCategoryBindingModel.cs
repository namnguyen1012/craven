﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding
{
    public class CreateCategoryBindingModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
