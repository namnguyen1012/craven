﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding
{
    public class CreateCustomerBindingModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Password { get; set; }
        public int Phone { get; set; }
        public string Place { get; set; }
        public DateTime? BirthDay { get; set; }
    }
}
