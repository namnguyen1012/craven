﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding
{
    public class CreateProductBindingModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public float ProductPrice { get; set; }
        public int CategoryId { get; set; }
        public IFormFile ProductImage { get; set; }
    }
}
