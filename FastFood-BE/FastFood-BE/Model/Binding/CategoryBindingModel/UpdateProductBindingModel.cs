﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding.CategoryBindingModel
{
    public class UpdateProductBindingModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public float ProductPrice { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
    }
}
