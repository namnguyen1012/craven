﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model.Binding.CategoryBindingModel
{
    public class UpdateCategoryBindingModel
    {
        public string CategoryName { get; set; }
    }
}
