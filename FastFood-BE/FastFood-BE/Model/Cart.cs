﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FastFood_BE.Model
{
    public class Cart
    {
        [Key]
        public int CartId { get; set; }
        public DateTime CartDate { get; set; } = DateTime.UtcNow;

        [JsonIgnore]
        public ICollection<CartDetail> CartDetails { get; set; }
    }
}
