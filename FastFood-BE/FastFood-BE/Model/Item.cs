﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastFood_BE.Model
{
    public class Item
    {
        public Product product { get; set; }
        public object Product { get; internal set; }
        public int Quantity { get; set; }
    }
}
