﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FastFood_BE.Data;
using FastFood_BE.Model;
using Newtonsoft.Json;
using FastFood_BE.Model.Binding;
using FastFood_BE.Helpers;
using System.Net;

namespace FastFood_BE.Controllers
{
    [Route("api/[controller]")]
    public class CartsController : BaseApiController
    {
        public CartsController(MainDBContext context) : base(context)
        {
        }

        // GET: api/Carts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cart>>> GetCarts()
        {
            return await db.Carts.ToListAsync();
        }

        // GET: api/Carts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cart>> GetCart(int id)
        {
            var cart = await db.Carts.FindAsync(id);

            if (cart == null)
            {
                return NotFound();
            }

            return cart;
        }

        // PUT: api/Carts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCart(int id, Cart cart)
        {
            if (id != cart.CartId)
            {
                return BadRequest();
            }

            db.Entry(cart).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Carts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("add-cart")]
        [HttpPost]
        public async Task<IActionResult> AddCart(CartBindingModel model)
        {
            var product = db.Products.FirstOrDefault(p => p.ProductId == model.ProductId);
            if (SessionHelper.GetObjectFromJson<List<CartDetail>>(HttpContext.Session, "Cart") == null)
            {
                Cart cart = new Cart();
                db.Carts.Add(cart);
                db.SaveChanges();
                CartDetail cartDetail = new CartDetail()
                {
                    CartId = cart.CartId,
                    ProductId = product.ProductId,
                    Quantity = model.Quantity == 0 ? model.Quantity : 1
                };
                db.CartDetails.Add(cartDetail);
                await db.SaveChangesAsync();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "Cart", cartDetail);
                return Success(cart);
            }
            else
            {
                List<CartDetail> Cart = SessionHelper.GetObjectFromJson<List<CartDetail>>(HttpContext.Session, "Cart");
                var index = CartExists(model.CartId);
                if(!index)
                {
                    for(int i = 0; i < Cart.Count(); i++)
                    {
                        Cart[i].Quantity++;
                    }
                    
                }
                else
                {
                    Cart.Add(new CartDetail { ProductId = product.ProductId, Quantity = 1 });
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "Cart", Cart);
                return Success(Cart);
            }
            return Error(HttpStatusCode.BadRequest, "Erro when add cart");
        }

        //public async Task<IActionResult> AddCart(CartBindingModel model)
        //{
        //    var session = HttpContext.Session.GetString("Cart");
        //    if (session == null)
        //    {
        //        var product = db.Products.Find(model.ProductId);
        //        var cartResult = new Cart() 
        //        { 
        //            userId = session,
        //            CartDetails = new List<CartDetail>()
        //        };

        //        var item = new CartDetail()
        //        {
        //            Product = product,
        //            Quantity = 1,
        //            Cart = db.Carts.Find(cartResult.CartId)
        //        };
        //        HttpContext.Session.SetString("Cart", JsonConvert.SerializeObject(item));
        //        db.Carts.Add(cartResult);
        //        cartResult.CartDetails.Add(item);
        //        await db.SaveChangesAsync();
        //        return Success(cartResult);
        //    }
        //    else
        //    {
        //        List<CartDetail> dataCart = JsonConvert.DeserializeObject<List<CartDetail>>(model.CartId.ToString());
        //        bool check = true;
        //        for(int i = 0; i < dataCart.Count; i++)
        //        {
        //            if(dataCart[i].Product.ProductId == model.ProductId)
        //            {
        //                dataCart[i].Quantity++;
        //                check = false;
        //            }
        //        }
        //        if (check)
        //        {
        //            dataCart.Add(new CartDetail
        //            {
        //                Product = db.Products.Find(model.ProductId),
        //                Quantity = 1
        //            });
        //        }
        //        HttpContext.Session.SetString("Cart", JsonConvert.SerializeObject(dataCart));
        //    }
        //    return RedirectToAction(session);
        //}


        //public async Task<ActionResult<Cart>> PostCart(Cart cart)
        //{
        //    db.Carts.Add(cart);
        //    await db.SaveChangesAsync();

        //    return CreatedAtAction("GetCart", new { id = cart.CartId }, cart);
        //}

        // DELETE: api/Carts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cart>> DeleteCart(int id)
        {
            var cart = await db.Carts.FindAsync(id);
            if (cart == null)
            {
                return NotFound();
            }

            db.Carts.Remove(cart);
            await db.SaveChangesAsync();

            return cart;
        }

        private bool CartExists(int id)
        {
            return db.Carts.Any(e => e.CartId == id);
        }
    }
}
