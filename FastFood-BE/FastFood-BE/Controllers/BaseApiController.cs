﻿using FastFood_BE.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FastFood_BE.Controllers
{
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        protected readonly MainDBContext db;

        public BaseApiController(MainDBContext db)
        {
            this.db = db;
        }

        protected IActionResult Success<T>(T dataResp)
        {
            return Ok(new 
            { 
                status = "success",
                sttCode = 200,
                message = "",
                data = dataResp
            });
        }

        protected IActionResult PostSuccess(string msg)
        {
            return Ok(new 
            {
                status = "succcess",
                sttCode = 200,
                message = msg
            });
        }

        protected IActionResult Error(HttpStatusCode sttCode, string msg = "")
        {
            var errResp = new
            {
                status = "failed",
                sttCode = sttCode,
                message = msg
            };

            switch (sttCode)
            {
                case HttpStatusCode.NotFound:
                    return NotFound(errResp);
                default:
                    return BadRequest(errResp);
            }
        }
    }
}
