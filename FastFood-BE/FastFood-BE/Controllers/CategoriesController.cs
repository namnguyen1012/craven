﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FastFood_BE.Data;
using FastFood_BE.Model;
using FastFood_BE.Model.Binding;
using FastFood_BE.Model.Binding.CategoryBindingModel;
using System.Net;

namespace FastFood_BE.Controllers
{
    [Route("api/categories")]
    public class CategoriesController : BaseApiController
    {

        public CategoriesController(MainDBContext context) : base(context)
        {
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var result = await db.Categories.ToListAsync();
            return Success(result);
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategory(int id)
        {
            var category = await db.Categories.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            return category;
        }

        // PUT: api/Categories/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, UpdateCategoryBindingModel category)
        {
            var result = db.Categories.Find(id);
            if (result == null)
            {
                return BadRequest();
            }
            
            try
            {
                db.Entry(result).Property(p => p.CategoryName).CurrentValue = string.IsNullOrEmpty(category.CategoryName) ? result.CategoryName : category.CategoryName;
                db.Entry(result).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Success(result);
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        // POST: api/Categories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("add")]
        [HttpPost]
        public async Task<IActionResult> PostCategory(CreateCategoryBindingModel model)
        {
            Category category = new Category
            {
                CategoryId = model.CategoryId,
                CategoryName = model.CategoryName
            };
            db.Categories.Add(category);
            await db.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = category.CategoryId }, category);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return Error(HttpStatusCode.NotFound,"Không tìm thấy sản phẩm hoặc đã bị xóa khỏi hệ thống");
            }

            db.Categories.Remove(category);
            await db.SaveChangesAsync();

            return PostSuccess("Xóa thành công");
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Any(e => e.CategoryId == id);
        }
    }
}
